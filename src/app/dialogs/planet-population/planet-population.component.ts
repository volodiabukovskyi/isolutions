import { Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';

import { Subscription } from 'rxjs';

import { PersonInterface } from 'src/app/interfaces/person.interface';
import { PlanetInterface } from 'src/app/interfaces/planet.interface';
import { PlanetsService } from 'src/app/services/planets.service';

@Component({
  selector: 'app-planet-population',
  templateUrl: './planet-population.component.html',
  styleUrls: ['./planet-population.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlanetPopulationComponent implements OnInit, OnDestroy {
  @ViewChild(MatTable) table!: MatTable<any>;

  displayedColumns: string[] = ['name', 'height', 'birth_year'];
  residensList: PersonInterface[] = [];
  isPlanetLoading = false;

  private subscriptions: Subscription = new Subscription();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PlanetInterface,
    public dialogRef: MatDialogRef<PlanetPopulationComponent>,
    private platentsService: PlanetsService
  ) { }

  ngOnInit(): void {
    this.getPeople();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private getPeople(): void { 
    this.isPlanetLoading = true;

    if (!this.data.residents.length) {
      return
    }

  
    const stream$ = this.platentsService.getPeople(this.data.residents).subscribe((resident: PersonInterface) => {
      this.residensList.push(resident);      
      this.isPlanetLoading = false;
      this.table.renderRows();
    });

    this.subscriptions.add(stream$)
  }
}
