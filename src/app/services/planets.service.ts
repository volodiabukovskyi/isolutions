import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { EMPTY, Observable, catchError, from, mergeMap } from 'rxjs';

import { MainPlanetInterface } from '../interfaces/planet.interface';
import { PersonInterface } from '../interfaces/person.interface';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(private http: HttpClient) { }

  getPlanets():Observable<MainPlanetInterface> {
    return this.http.get<MainPlanetInterface>('https://swapi.dev/api/planets');
  }

  getPeople(urls: string[]): Observable<PersonInterface> {
    const makeRequest = (url: string) => this.http.get<PersonInterface>(url).pipe(
      catchError(() => EMPTY));

     const films$: Observable<PersonInterface> = from(urls).pipe(
      mergeMap(url => makeRequest(url))
    );

    return films$
  }
}
