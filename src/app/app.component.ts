import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subscription, finalize, map } from 'rxjs';

import { PlanetPopulationComponent } from './dialogs/planet-population/planet-population.component';
import { PlanetInterface } from './interfaces/planet.interface';
import { MainPlanetInterface } from './interfaces/planet.interface';
import { PlanetsService } from './services/planets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'iSolutions';
  displayedColumns: string[] = ['name','diameter', 'climate', 'population'];
  platentsList: PlanetInterface[] = [];
  selectedPlanet: PlanetInterface[] = [];
  isPlanetLoading = false;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private planetsService: PlanetsService,
    private dialog: MatDialog
  ){}

  ngOnInit(): void {
    this.getPlanets();
  }

  selectPlanet(planet: PlanetInterface): void {
    this.selectedPlanet = [ planet ];
  }

  openPalentDetail(planet: PlanetInterface): void {
    this.dialog.open(PlanetPopulationComponent, {
      data: planet,
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private getPlanets(): void {
    this.isPlanetLoading = true;
    const stream$ = this.planetsService.getPlanets().pipe(
      map((data: MainPlanetInterface) => data.results),
      finalize(()=> this.isPlanetLoading = false)
    ).subscribe((planets: PlanetInterface[]) => {
      this.platentsList = planets; 
    })

    this.subscriptions.add(stream$)
  }
}
